function activarPestana(elemento) {
    var pestanas = document.querySelectorAll('.pestanas .pestaña');
    var mensajeNoDisponible = document.querySelector('.mensaje-no-disponible');
    var titulo = document.querySelector('.titulo');
    var parrafo = document.querySelector('.parrafo');
    var mensajeFinal = document.querySelector('.texto-final');
    var galeria = document.querySelector('.galeria');

    pestanas.forEach(function (pestana) {
        pestana.classList.remove('activa');
    });

    elemento.classList.add('activa');

    if (elemento.textContent.trim() === 'Aliados') {
        galeria.style.display = 'grid';
        mensajeNoDisponible.style.display = 'none';
        titulo.style.display = 'block';
        parrafo.style.display = 'block';
        mensajeFinal.style.display = 'block';
    } else if (elemento.textContent.trim() === 'Descubrir') {
        galeria.style.display = 'none';
        mensajeNoDisponible.style.display = 'block';
        titulo.style.display = 'none';
        parrafo.style.display = 'none';
        mensajeFinal.style.display = 'none';
    }
}
